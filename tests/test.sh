#!/bin/bash

OUTPUT=$1

echo "FILE,SIZE,SHEX-REC-SIMPLE,SHEX-REC-MEM,SHEX-REFINE,SHACL minus LOADING,SHACL,SHACL-LOAD-GRAPH-ONLY" >> $OUTPUT
# traverser toutes les lignes d'un fichier
for file in `ls data-files`;
do
    result=""
    size=""
    
    # run the shex algorithms
    for algorithm in -shex-rec-simple -shex-rec-mem -shex-refine;
    do
	java -jar ../shexjbench/target/shexjbench-0.1.jar data-files/$file $algorithm > /tmp/log
	size_time=`tail -n1 /tmp/log`
	size=`echo $size_time | cut -d: -f1`
	time=`echo $size_time | cut -d: -f2`
	result="${result} ${time},"
    done
    result="${size}, ${result}"

    # run the shacl and load graph only for shacl
    java -jar ../shexjbench/target/shexjbench-0.1.jar data-files/$file -shacl > /tmp/log
    size_time=`tail -n1 /tmp/log`
    valid_plus_load_time=`echo $size_time | cut -d: -f2`

    java -jar ../shexjbench/target/shexjbench-0.1.jar data-files/$file -shacl-load-graph-only > /tmp/log
    size_time=`tail -n1 /tmp/log`
    load_time=`echo $size_time | cut -d: -f2`

    time=`expr ${valid_plus_load_time} - ${load_time}`
    result="${result} ${time}, ${valid_plus_load_time}, ${load_time}"

    file_name=`echo $file | cut -d/ -f2`
    result="${file_name}, ${result}"
    echo $result >> $OUTPUT
done
