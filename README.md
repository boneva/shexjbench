# About
This project aims at comparing the efficiency of the different [shex-java](https://github.com/iovka/shex-java) validation algorithms, 
and also to compare their efficiency with [SHACL validation with RDF4J](https://rdf4j.org/documentation/programming/shacl/).

In this first version we allow to run one of the supported algorithms on some graph using a default schema, and print the validation times.
The default schema is generated automatically from the graph using [ShapeDesigner](https://gitlab.inria.fr/jdusart/shexjapp).
Thus the ShEx and SHACL schemas used for the same graph are guaranteed to be semantically equivalent.

# Usage
Use maven to build a jar
```mvn compile assembly:single```

## Single run
```java -jar <jar file> <data file> <validation algorithm> [-v]```

Where `<validation algorithm>` refers to an option that is one of the following
```
    -shacl : SHACL validation
    -shacl-load-graph-only : Loads the data in ShaclSail w/o validation. To be substracted from -shacl validation.
    -shex-rec-mem : ShEx recursive validation with memorization
    -shex-rec-simple : SdEx recursive validation w/o memorization
    -shex-refine : ShEx refine validation
```
The -v option allows to print the generated schema.

For SHACL validation time, we advise to substract the result of -shacl-load-graph-only from the result of -shacl.
Indeed, SHACL validation is done while triples are added to the RDF Sail repository. 
In -shacl-load-graph-only the triples are added w/o presence of SHACL schema, so substracting it allows in principle to see what is the overhead of SHACL validation.

## Running tests on a benchmark
A simple bash script `tests/test.sh` allows to run all algorithms on a set of files and output the results in a CSV format.
```
$ test.sh output.csv
```
would run all algorithms on all files in the `tests/data-files` folder and print the validation times (in milliseconds) in output.csv

This can take a lot of time for big graphs. Indeed, the schema is generated at each run.
Generating the schemas once is left for next version.

## Limitations
- This simple first version does not reflect the same usage. 
SHACL validation with RDF4J is possible only on graphs in a Sail repository (as far as I understand it),
while the simplest use of shex-java is done on a graph model in memory and in this version we use this one.
Note that we do use a MemoryStore for SHACL (so no disk access overhead).
With shex-java it is possible to store the graph in a repository in order to perform the test in best possible similar situations. 
This modification is left for next version of the benchmark tool.
- Each test is run separately on purpose. 
Indeed, if running all the algorithms within the same execution, then execution times are infuenced by the order in which the different algorithms are run.
This is probably due to JVM overheads of some kind. 
In this first version we run every test alone in its own JVM in order to avoid the above problem. 
- Schema generation relies on rdf:type; if the graph nodes do not haver rdf:type, then the generated schema would be empty.

# Known issues
- Bug of SHACL schema generation (due to bug in ShapeDesigner): if some set of nodes of the graph 
have multiple rdf:type with exactly the same classes, then SHACL schema generation fails. 



