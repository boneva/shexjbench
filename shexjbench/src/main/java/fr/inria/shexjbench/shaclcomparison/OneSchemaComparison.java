package fr.inria.shexjbench.shaclcomparison;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import fr.inria.lille.shexjava.exception.CyclicReferencesException;
import fr.inria.lille.shexjava.exception.NotStratifiedException;
import fr.inria.lille.shexjava.exception.UndefinedReferenceException;
import fr.inria.lille.shexjava.pattern.PatternParsing;
import fr.inria.lille.shexjava.pattern.abstrt.Pattern;
import fr.inria.lille.shexjava.pattern.indications.PatternInstantiation;
import fr.inria.lille.shexjava.pattern.indications.SHACLFromPatternConstructor;
import fr.inria.lille.shexjava.pattern.indications.ShexFromPatternConstructor;
import fr.inria.lille.shexjava.schema.Label;
import fr.inria.lille.shexjava.schema.ShexSchema;
import fr.inria.lille.shexjava.schema.abstrsynt.ShapeExpr;
import fr.inria.lille.shexjava.validation.RecursiveValidation;
import fr.inria.lille.shexjava.validation.RecursiveValidationWithMemorization;
import fr.inria.lille.shexjava.validation.RefineValidation;
import fr.inria.lille.shexjava.validation.ValidationAlgorithm;
import org.apache.commons.rdf.api.Graph;
import org.apache.commons.rdf.api.IRI;
import org.apache.commons.rdf.api.RDFTerm;
import org.apache.commons.rdf.api.Triple;
import org.apache.commons.rdf.rdf4j.RDF4J;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.impl.TreeModel;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.repository.sail.SailRepositoryConnection;
import org.eclipse.rdf4j.rio.*;
import org.eclipse.rdf4j.sail.Sail;
import org.eclipse.rdf4j.sail.memory.MemoryStore;
import org.eclipse.rdf4j.sail.shacl.ShaclSail;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Iovka Boneva
 */
public class OneSchemaComparison {

    static final String BASE_IRI = "http://inria.fr/shexjbench/";
    static final Pattern SHAPE_EXTRACTION_PATTERN;
    static {
        Pattern temp = null;
        PatternParsing parser = new PatternParsing();
        parser.setPrefixesMap(Collections.emptyMap());
        try {
            temp = parser.getPattern(new ByteArrayInputStream("{a [__] ; ~ __ }".getBytes()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        SHAPE_EXTRACTION_PATTERN = temp;
    }

    static class Pair<F,S> {
        final F first;
        final S second;
        Pair(F first, S second) {
            this.first = first;
            this.second = second;
        }
    }

    static RDF4J factory = new RDF4J();

    // Loads the data from file in model (used for shex validation), and in dataRepository (used for shacl validation)
    static Model loadData(File path) throws RDFParseException, UnsupportedRDFormatException, IOException {
        Optional<RDFFormat> format = Rio.getParserFormatForFileName(path.getName());
        if (!format.isPresent())
            throw new UnsupportedRDFormatException("File format not found");

        FileInputStream fin = new FileInputStream(path);
        return Rio.parse(fin, BASE_IRI, format.get()); // TODO should base iri be this L
    }


    static List<Pair<PatternInstantiation,IRI>> constructAbstractSchema (Model data) {
        List<Pair<PatternInstantiation,IRI>> result = new ArrayList<>();

        Set<Value> rdfTypes = data.filter(null, RDF.TYPE, null).objects();
        for (Value v: rdfTypes) {
            Resource rdfType = (Resource) v;
            List<RDFTerm> sample = data.filter(null, RDF.TYPE, rdfType).subjects().stream().map(it -> factory.asRDFTerm(it)).collect(Collectors.toList());
            PatternInstantiation pi = new PatternInstantiation(SHAPE_EXTRACTION_PATTERN, sample, factory.asGraph(data));
            result.add(new Pair<PatternInstantiation, IRI>(pi, (IRI) factory.asRDFTerm(rdfType)));
        }
        return result;
    }

    static Pair<ShexSchema,Map<Label,IRI>> constructShexSchema (List<Pair<PatternInstantiation,IRI>> rules) {
        Map<Label, ShapeExpr> shexSchema = new HashMap<>();
        Map<Label,IRI> shapeLabelToRdfTypeMap = new HashMap<>();

        for (Pair<PatternInstantiation,IRI> rulePair : rules) {
            ShexFromPatternConstructor shexConstructor = new ShexFromPatternConstructor();
            ShapeExpr shexpr = shexConstructor.construct(rulePair.first);
            String rdfTypeIriString = rulePair.second.getIRIString();
            String rdfTypeSimpleName = rdfTypeIriString.substring(rdfTypeIriString.lastIndexOf('/')+1, rdfTypeIriString.length());
            Label label = new Label(factory.createIRI(BASE_IRI + rdfTypeSimpleName + "Shape"));
            shexpr.setId(label);
            shexSchema.put(label, shexpr);
            shapeLabelToRdfTypeMap.put(label,rulePair.second);
        }
        try {
            return new Pair(new ShexSchema(shexSchema), shapeLabelToRdfTypeMap);
        } catch (UndefinedReferenceException | CyclicReferencesException | NotStratifiedException e) {
            e.printStackTrace();
            return null;
        }
    }

    static Model constructShaclSchema (List<Pair<PatternInstantiation,IRI>> rules) {
        Model result = new TreeModel();

        for (Pair<PatternInstantiation,IRI> rulePair: rules) {
            SHACLFromPatternConstructor shaclConstructor = new SHACLFromPatternConstructor(BASE_IRI);
            result.addAll(shaclConstructor.construct(rulePair.first));
        }
        return result;
    }

    static long fakeValidateShacl (Model shaclSchema, Model data) {
        Sail sail = new MemoryStore();
        SailRepository dataRepository = new SailRepository(sail);
        dataRepository.init();

        long start = System.currentTimeMillis();
        try (SailRepositoryConnection connection = dataRepository.getConnection()) {
            connection.begin();
            connection.add(data);
            connection.commit();
        }
        long end = System.currentTimeMillis();
        return end - start;
    }

    static long validateShacl (Model shaclSchema, Model data) {
        ShaclSail shaclSail = new ShaclSail(new MemoryStore());
        SailRepository dataRepository = new SailRepository(shaclSail);
        dataRepository.init();

        try (SailRepositoryConnection connection = dataRepository.getConnection()) {
            connection.begin();
            connection.add(shaclSchema, SimpleValueFactory.getInstance().createIRI("http://rdf4j.org/schema/rdf4j#SHACLShapeGraph"));
            connection.commit();
        }

        long start = System.currentTimeMillis();
        try (SailRepositoryConnection connection = dataRepository.getConnection()) {
            connection.begin();
            connection.add(data);
            connection.commit();
        }
        long end = System.currentTimeMillis();
        return end - start;
    }

    static Pair<Boolean,Long> validateShex (ValidationAlgorithm algorithm, Graph dataGraph, Map<Label, IRI> shexLabelToRdfTypeMap) {
        long start = System.currentTimeMillis();
        boolean validationResult = true;
        for (Map.Entry<Label,IRI> e: shexLabelToRdfTypeMap.entrySet()) {
            for (Triple t: dataGraph.iterate(null, factory.asRDFTerm(RDF.TYPE), e.getValue())) {
                validationResult = validationResult && algorithm.validate(t.getSubject(), e.getKey());
            }
        }
        long end = System.currentTimeMillis();
        return new Pair<>(validationResult, end-start);
    }

    enum ShexAlgorithmType {
        RECSIMPLE, RECMEM, REFINE;
        String description () {
            switch (this) {
                case RECSIMPLE: return "recursive simple";
                case RECMEM: return "recursive with memorization";
                case REFINE: return "refine";
                default: throw new UnsupportedOperationException();
            }
        }
        ValidationAlgorithm createAlgorithm(ShexSchema schema, Graph dataGraph) {
            switch (this) {
                case RECSIMPLE: return new RecursiveValidation(schema, dataGraph);
                case RECMEM: return new RecursiveValidationWithMemorization(schema, dataGraph);
                case REFINE: return new RefineValidation(schema, dataGraph);
                default: throw new UnsupportedOperationException();
            }
        }
    };

    static Long printValidateShex (ShexAlgorithmType type, Graph dataGraph, ShexSchema schema, Map<Label, IRI> labelToRdfTypeMap) {

        System.out.print(String.format("ShEx validation with %s algorithm ... \n", type.description()));
        ValidationAlgorithm algorithm = type.createAlgorithm(schema, dataGraph);
        Pair<Boolean,Long> shexValidationResultAndTime = validateShex(algorithm, dataGraph, labelToRdfTypeMap);
        System.out.println(String.format("(%s) done in %d ms",
                                            shexValidationResultAndTime.first ? "valid" : "invalid",
                                            shexValidationResultAndTime.second));
        return shexValidationResultAndTime.second;
    }

    static Map<String,String> supportedAlgorithms = new LinkedHashMap<>();
    static {
        supportedAlgorithms.put("-shacl", "SHACL validation");
        supportedAlgorithms.put("-shacl-load-graph-only", "Loads the data in ShaclSail w/o validation. To be substracted from -shacl validation.");
        supportedAlgorithms.put("-shex-rec-mem", "ShEx recursive validation with memorization");
        supportedAlgorithms.put("-shex-rec-simple", "SdEx recursive validation w/o memorization");
        supportedAlgorithms.put("-shex-refine", "ShEx refine validation");
    }

    static String usage;

    static {
        StringBuilder s = new StringBuilder();
        s.append("Validates an rdf data graph against an automatically extracted ShEx or SHACL schema.\n");
        s.append("Prints the validation time.\n");
        s.append("USAGE:\n");
        s.append("  First argument [mandatory] : rdf data file\n");
        s.append("  Second argument [mandatory] : the validation algorithm among\n");
        for (Map.Entry<String, String> e : supportedAlgorithms.entrySet()) {
            s.append(String.format("    %s : %s\n", e.getKey(), e.getValue()));
        }
        s.append("  Third argument [optional] : -v option would print the generated schema\n");
        usage = s.toString();
    }

    public static void main (String[] args) {

        Model data = null;
        String validationAlgorithm = null;
        boolean verbose = false;

        if (args.length < 2 || args.length > 3) {
            System.out.println(usage);
            System.exit(0);
        }

        // Load data file and process arguments
        try {
            validationAlgorithm = args[1];
            if (! supportedAlgorithms.keySet().contains(validationAlgorithm))
                throw new RuntimeException();

            File dataFile = new File(args[0]);
            if (!dataFile.exists())
                throw new RuntimeException();
            data = loadData(dataFile);

            if (args.length > 2 && args[2].equals("-v"))
                verbose = true;
        } catch (Exception e) {
            System.out.println(usage);
            e.printStackTrace();
            System.exit(0);
        }

        boolean useShexSchema = false;
        boolean useShaclSchema = false;
        if (validationAlgorithm.startsWith("-shex"))
            useShexSchema = true;
        else if (validationAlgorithm.equals("-shacl"))
            useShaclSchema = true;

        /*
        Set<String> options = Arrays.asList(args).stream()
                        .map(s -> s.toLowerCase())
                        .filter(s -> s.startsWith("-"))
                        .collect(Collectors.toSet());
        if (options.isEmpty() || (options.size()==1 && options.contains("-v"))) {
            options.add("-rec"); options.add("-shacl");
        }
        */


        // Create the schema
        Pair<ShexSchema, Map<Label, IRI>> shexSchemaAndMap = null;
        Model shaclSchema = null;
        System.out.print("Creating the schema. This may take time for big rdf graphs ... ");
        List<Pair<PatternInstantiation,IRI>> abstractSchema = constructAbstractSchema(data);
        if (useShexSchema) shexSchemaAndMap= constructShexSchema(abstractSchema);
        if (useShaclSchema) shaclSchema = constructShaclSchema(abstractSchema);
        System.out.println("done.");

        // Print schemas
        if (verbose) {
            if (useShaclSchema) printShexSchema(shexSchemaAndMap.first);
            if (useShaclSchema) printShaclSchema(shaclSchema);
        }

        Graph dataGraph = factory.asGraph(data);

        // Map<String, Long> validationTime = new HashMap<>(4);
        long validationTime = -1;
        if (validationAlgorithm.equals("-shex-rec-mem")) {
            validationTime = printValidateShex(ShexAlgorithmType.RECMEM, dataGraph, shexSchemaAndMap.first, shexSchemaAndMap.second);
        }
        if (validationAlgorithm.equals("-shex-rec-simple")) {
            validationTime = printValidateShex(ShexAlgorithmType.RECSIMPLE, dataGraph, shexSchemaAndMap.first, shexSchemaAndMap.second);
        }
        if (validationAlgorithm.equals("-shex-refine")) {
            validationTime = printValidateShex(ShexAlgorithmType.REFINE, dataGraph, shexSchemaAndMap.first, shexSchemaAndMap.second);
        }
        if (validationAlgorithm.equals("-shacl")) {
            System.out.print("SHACL validation ... \n");
            validationTime = validateShacl(shaclSchema, data);
            System.out.println("done in " + validationTime + " ms.");
        }
        if (validationAlgorithm.equals("-shacl-load-graph-only")) {
            System.out.print("Loading graph for SHACL validation w/o runnig validation ... ");
            validationTime = fakeValidateShacl(shaclSchema, data);
            System.out.println("done in " + validationTime + " ms.");
        }
        System.out.println(validationTime);
    }

    static void printShexSchema (ShexSchema schema) {
        System.out.println("---------- ShEx schema ----------");
        ShExCSerializer shexcSerializer = new ShExCSerializer();
        for (Map.Entry<Label, ShapeExpr> rule : schema.getRules().entrySet()) {
            System.out.println(rule.getKey());
            System.out.println(shexcSerializer.convertShapeExpr(rule.getValue()) + "\n");
        }
    }

    static void printShaclSchema (Model schema) {
        System.out.println("---------- SHACL schema ----------");
        Rio.write(schema, System.out, RDFFormat.TURTLE);
    }

}
